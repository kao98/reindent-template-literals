const assert = require('assert/strict')
const { reindent, reindentTag } = require('reindent-template-literals')

const usingReindentMethod = reindent(`
  This line should not be indented
    This line should be :)
`)

const usingReindentTag = reindentTag`
  This line should not be indented
    This line should be :)
`

const unindentedString = `\
This line should not be indented
  This line should be :)`

assert.strictEqual(unindentedString, usingReindentMethod)
assert.strictEqual(unindentedString, usingReindentTag)
