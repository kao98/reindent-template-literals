# Acceptance tests for reindent-template-literals

Those tests make sure the module works in a node project with CommonJS, ES Modules and Typescript.

Use `npm link` between the source of reindent-template-literals and the tests folder.

Then `npm install-test` to execute all acceptance tests

```shell
# considering we are at tests/
cd ..
npm link
cd tests
npm link reindent-template-literals
npm install-test
```
