import assert from 'assert/strict'
import { reindent, reindentTag } from '../src/index.js'
import { describe, it } from 'minispec'

describe('reindent', async () => {
  it('removes the initial indentation', async () => {
    const expected = `Not indented string`
    const actual = reindent(`    Not indented string`)

    assert.strictEqual(actual, expected)
  })

  it('removes the first line if it is empty', async () => {
    const expected = `Not indented string`
    const actual = reindent(`
      Not indented string`)

    assert.strictEqual(actual, expected)
  })

  it('removes the last line if it is empty', async () => {
    const expected = `Not indented string`
    const actual = reindent(`
      Not indented string
    `)

    assert.strictEqual(actual, expected)
  })

  it('preserves the indentation within the string', async () => {
    const expected = `\
Not indented
  First level
  Still first level
    Second level
  Back to first level`

    const actual = reindent(`
      Not indented
        First level
        Still first level
          Second level
        Back to first level
      `)

    assert.strictEqual(actual, expected)
  })

  it('consider the indentation of the first non-blank line', async () => {
    const expected = `\


Not indented
  First level
  Still first level
    Second level
  Back to first level`

    const actual = reindent(`


      Not indented
        First level
        Still first level
          Second level
        Back to first level
      `)

    assert.strictEqual(actual, expected)
  })

  it('falls-back to initial indentation for lines less indented than the first one', async () => {
    const expected = `\
Not indented
Should be reindented`

    const actual = reindent(`
        Not indented
      Should be reindented
    `)

    assert.strictEqual(actual, expected)
  })
})

describe('reindentTag', async () => {
  it('reindents the string', async () => {
    const expected = `\
Not indented
  First level
    Second level`

    const actual = reindentTag`
      Not indented
        First level
          Second level
      `

    assert.strictEqual(actual, expected)
  })

  it('interpolates the template', async () => {
    const hello = 'Hello'
    const you = 'World'
    const clap = '👏'
    const expected = `${hello}, ${you}! ${clap}`

    const actual = reindentTag`
      ${hello}, ${you}! ${clap}`

    assert.strictEqual(actual, expected)
  })
})

describe('The usage example of the README', async () => {
  it('should pass', async () => {
    const reindented = reindent(`
      Feature: reindent template strings
        Scenario: Scenario #1
          Given ...
          When ...
          Then ...

    `)

    const expected = `\
Feature: reindent template strings
  Scenario: Scenario #1
    Given ...
    When ...
    Then ...
`

    assert.strictEqual(reindented, expected)
  })
})
