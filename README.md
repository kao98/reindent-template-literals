# Reindent template literals

Do you have some indentation issues in your source code due to template
literals?

```typescript
describe('something', () => {
  it('works', () => {
    const expectedResult = `\
┌───────────────────┐
│ The answer is: 42 │
└───────────────────┘`

    assert.deepStrictEqual(ComputeFor7point5MillionYears(), expectedResult)
  })
})
```

`reindent-template-literals` can help you with this: the following snippet will
act exactly the same as the previous one:

```typescript
describe('something', () => {
  it('works', () => {
    const expectedResult = reindent`
      ┌───────────────────┐
      │ The answer is: 42 │
      └───────────────────┘
      `

    assert.deepStrictEqual(ComputeFor7point5MillionYears(), expectedResult)
  })
})
```

## Table of Content

- [Description and examples](#description-and-examples)
- [Installation](#installation)
- [Usage](#usage)
  - [With Typescript or ES-Modules](#with-typescript-or-es-modules)
  - [Using CommonJS](#using-commonjs)

## Description and examples

The `reindent` method take the indent of the first line and will remove that
indent from the entire template. If the first and/or last line are empty, they
are ignored.

In the following example, notice how the whole reindented string does not break
the indentation of the source code compared to a regular template literal.

Note how the first line of the regular template literal must be escaped to be
ignored. This is not the case when using `reindent`.

Note also the blank line at the end: the expected string actually ends with a
blank line. `reindent` make that final blank line explicit.

```typescript
  const reindented = reindent(`
    Feature: reindent template strings
      Scenario: Scenario #1
        Given ...
        When ...
        Then ...

  `)

  const expected = `\
Feature: reindent template strings
  Scenario: Scenario #1
    Given ...
    When ...
    Then ...
`

  assert.strictEqual(reindented, expected)
```

`reindent-template-literals` comes with a `reindent` method, but also with a
tag function. Template literals will be interpolated with both. See
[Usage](#usage) for more details.

## Installation

```shell
npm install reindent-template-literals
```

## Usage

Two ways are available: as a function, or as a [tag function](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals#tagged_templates).

It is up to you to choose between the function and the tag function. The later
may look nicer, but it reimplements the template interpolation so it may be less
efficient.

### With Typescript or ES-Modules

As a tag function:

```typescript
import { reindentTag as reindent } from 'reindent-template-literals'

console.log(reindent`
  ┌────────────────────────┐
  │ The answer is: ${ 42 } │
  └────────────────────────┘
`)
```

As a function:

```typescript
import { reindent } from 'reindent-template-literals'

console.log(
  reindent(`
    ┌────────────────────────┐
    │ The answer is: ${ 42 } │
    └────────────────────────┘
  `)
)
```
### Using CommonJS

As a tag function:

```javascript
const { reindentTag: reindent } = require('reindent-template-literals')

console.log(reindent`
  ┌────────────────────────┐
  │ The answer is: ${ 42 } │
  └────────────────────────┘
`)
```

As a function:

```javascript
const { reindent } = require('reindent-template-literals')

console.log(
  reindent(`
    ┌────────────────────────┐
    │ The answer is: ${ 42 } │
    └────────────────────────┘
  `)
)
```
