/**
 * Reindent the template literal based on the indentation of its first line.
 *
 * It removes the first line if it is empty so it is not needed for it to be escaped.
 *
 * @example
 * {
 *   const reindented = reindentTag`
 *     Expected output
 *       Expected indented output
 *       Another line
 *     `
 *
 *   const expected = `\
 *  Expected output
 *    Expected indented output
 *    Another line
 *  `
 *    expect(reindented).to.eql(expected)
 * }
 */
export function reindentTag(
  templateData: TemplateStringsArray,
  ...expressionValues: string[]
): string {
  return reindent(
    interpolate(templateData, ...expressionValues)
  )
}

/**
 * Reindent the string based on the indentation of its first line.
 *
 * It removes the first line if it is empty so it is not needed for it to be escaped.
 *
 * @example
 * {
 *   const reindented = reindent(`
 *     Expected output
 *       Expected indented output
 *       Another line
 *     `)
 *
 *   const expected = `\
 *  Expected output
 *    Expected indented output
 *    Another line
 *  `
 *    expect(reindented).to.eql(expected)
 * }
 */
export function reindent(original: string): string {
  const lines = original.replace(/^\n/, '').split(`\n`)
  const numberOfSpaceToRemove = findFirstNonBlankLine(lines).search(/\S|$/)
  const spacesToRemove = Array(numberOfSpaceToRemove + 1).join(' ')

  const reindentedLines = lines.map((line) => {
    if (line.startsWith(spacesToRemove)) {
      return line.replace(spacesToRemove, '')
    }

    return line.trimStart()
  })

  return removeLastLineIfBlank(reindentedLines).join('\n')
}

function interpolate(
  templateData: TemplateStringsArray,
  ...expressionValues: string[]
): string {
  const reducer = (accumulator: string, currentValue: string, currentIndex: number) => {
    if (currentIndex < expressionValues.length) {
      return `${accumulator}${currentValue}${expressionValues[currentIndex]}`
    }

    return `${accumulator}${currentValue}`
  }

  return templateData.reduce(reducer, '')
}

function findFirstNonBlankLine(lines: string[]): string {
  return lines.find((line) => line.trim() !== '') || lines[0]
}

function removeLastLineIfBlank(lines: string[]): string[] {
  const lastLine = lines[lines.length - 1]

  if (lastLine.trim().length) {
    return lines
  }

  return lines.slice(0, -1)
}
