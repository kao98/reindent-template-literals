
# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased] - 2021-07-29

### Added

### Changed

### Fixed

## [v1.1.0] - 2021-07-29

- Enhance README.md with a description and more example.

### Changed

- If the last line is blank, it is removed. That avoid having the string ending with
  an undesired new-line character.

- It uses the indentation of the first non-blank lines rather than the first line.

## [v1.0.0] - 2021-07-15

Initial release

[v1.1.0]: https://gitlab.com/kao98/reindent-template-literals/-/tags/v1.1.0
[v1.0.0]: https://gitlab.com/kao98/reindent-template-literals/-/tags/v1.0.0
